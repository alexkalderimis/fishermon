# fishermon

A hook monitoring tool.

## What does it do?

Watch for hook data, show it in the browser.  

## How do I run this?

```
stack run
```

## Who wrote this?

Alex Kalderimis, open source code under MIT 

