{-# LANGUAGE OverloadedStrings, QuasiQuotes #-}
module Fishermon (runApp, app) where

import Data.Char (toLower)
import qualified Data.HashMap.Strict as M
import qualified Data.Text.Lazy as T
import Control.Monad.IO.Class (liftIO)
import Control.Monad
import Data.Maybe
import Data.String (fromString)
import Data.IORef
import qualified Text.Blaze.Html5 as H
import Text.Blaze.Html5
import Text.Blaze.Html5.Attributes as A hiding (id)
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import qualified Data.ByteString.Lazy as L
import Text.Read (readMaybe)
import           Data.Aeson (Value(..), object, (.=))
import           Network.Wai (Application)
import Network.Wai.Handler.Warp (defaultSettings, setPort, setHost)
import System.Environment (lookupEnv)
import qualified Web.Scotty as S

type DB = IORef [Value]

app :: IO Application
app = (app' <$> newDB) >>= S.scottyApp

runApp :: IO ()
runApp = do
  db <- newDB
  port <- fmap (setPort . fromMaybe 8080 . (>>= readMaybe)) (lookupEnv "PORT")
  host <- fmap (maybe id (setHost . fromString)) (lookupEnv "HOST")
  let opts = port . host $ defaultSettings
  S.scottyOpts (S.Options 1 opts) (app' db)

app' :: DB -> S.ScottyM ()
app' db = do
  S.get "/" $ do
    n <- length <$> liftIO (readIORef db)
    format <- fromMaybe "text/plain" <$> S.header "Accept"
    if "text/html" `T.isInfixOf` format
       then liftIO (readIORef db) >>= S.raw . showResultsHtml
       else S.text $ "hello. I have received " <> fromString (show n) <> " messages\n"

  S.post "/hook" $ do
    receiveHook db =<< S.jsonData
    S.text "ok"

  S.post "/clear" $ do
    liftIO (writeIORef db [])
    S.redirect "/"

newDB :: IO DB
newDB = newIORef []

receiveHook db value = liftIO . atomicModifyIORef' db $ \received -> (value : received, ())

showResultsHtml hooks = do
  renderHtml . docTypeHtml $ do
    H.head $ do
      H.title "Fishermon"
      bootstrap
    H.body ! A.class_ "container" $ do
      let btnAttrs = mconcat . concat $ [ [A.class_ "btn btn-primary"]
                                    , [ A.disabled "" | null hooks ]
                                    ]
      H.h1 "Fishermon"
      H.form ! A.method "POST"
             ! A.action "/clear"
             ! (if null hooks then A.disabled "" else mempty)
             $ (H.button ! btnAttrs $ H.text "clear")
      H.p $ do
        H.text $ "Messages received: " <> fromString (show $ length hooks)
        forM_ (zip [1..] hooks) renderHook

renderHook (i, hook) = do
    H.h2 $ H.string ("Hook " <> show i)
    go hook
  where
    go hook = case hook of
      Object o -> H.table ! tableClass $ forM_ (M.toList o) $ \(k, v) -> H.tr $ do
                    H.td . H.code $ H.text k
                    H.td (go v)
      Array vs -> H.table $ mapM_ go vs 
      String t -> H.text t
      Number n -> H.string (show n)
      Bool b -> H.code $ H.string (fmap toLower $ show b)
      Null -> H.code "null"

tableClass = A.class_ "table table-sm table-bordered"
                
bootstrap = H.link
  ! A.href "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
  ! A.rel "stylesheet"
  ! H.customAttribute "integrity" "sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
  ! H.customAttribute "crossorigin" "anonymous"
